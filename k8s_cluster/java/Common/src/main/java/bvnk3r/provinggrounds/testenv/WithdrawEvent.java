package bvnk3r.provinggrounds.testenv;

import java.math.BigDecimal;

public class WithdrawEvent extends AbstractFinancialEvent {
    public WithdrawEvent() {
        super();
    }

    public WithdrawEvent(String originService, String destinationService, BigDecimal sum) {
        super(originService, destinationService, sum);
    }
}
