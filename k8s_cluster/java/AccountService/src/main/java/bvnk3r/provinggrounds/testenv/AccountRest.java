package bvnk3r.provinggrounds.testenv;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;
import java.util.List;

@RestController
public class AccountRest {

    private static Logger log = LoggerFactory.getLogger(AccountRest.class);

    @Autowired
    private RemoteEventPublisher eventPublisher;
    @Autowired
    private AccountDAO dao;
    @Autowired
    private AccountRepository repo;

    @RequestMapping("/create")
    public void create(@RequestParam("client_id") Integer clientId) {
        log.info("generating a card number");
        dao.create(clientId);
    }

    @RequestMapping("/fund/{id}")
    public boolean fund(@PathVariable Integer id, @RequestParam BigDecimal sum) {
        try {
            return dao.addBalance(id, sum.abs());
        } finally {
            eventPublisher.publishEvent(new FundEvent("AccountService", "HistoryService", sum));
        }
    }

    @RequestMapping("/checkout/{id}")
    public boolean checkout(@PathVariable Integer id, @RequestParam BigDecimal sum) {
        BigDecimal negativeSum = sum.abs().negate();
        try {
            return dao.addBalance(id, negativeSum);
        } finally {
            eventPublisher.publishEvent(new WithdrawEvent("AccountService", "HistoryService", negativeSum));
        }
    }

    @RequestMapping("/get/{clientId}")
    public List<? extends Account> getByClient(@PathVariable Integer clientId)
    {
        return repo.findByClientId(clientId);
    }
}
