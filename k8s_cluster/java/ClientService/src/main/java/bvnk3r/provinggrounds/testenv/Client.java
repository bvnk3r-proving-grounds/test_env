package bvnk3r.provinggrounds.testenv;

public interface Client {
    Integer getId();
    String getName();
}
