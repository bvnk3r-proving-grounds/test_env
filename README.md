# test_env
Learning Docker &amp; K8s

Docker (docker-compose):
* Postgres
* Kafka
* Redis
* RabbitMQ
* Vault

K8s cluster:
* Monitoring
  1. Prometheus
  2. Grafana (admin/prom-operator)
* Logging
  1. Elastic
  2. Kibana


Vault config (vault.json):

```json
{
  "ui": true,
  "backend": {"file": {"path": "/vault/file"}},
  "listener": {"tcp": {"address": "0.0.0.0:8200", "tls_disable": 1}},
  "default_lease_ttl": "168h",
  "max_lease_ttl": "0h"
}
```
